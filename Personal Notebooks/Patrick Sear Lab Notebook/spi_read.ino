#include <SPI.h>



#define DRDY 31 //PC6 = DRDY = pin 31
#define UPPER 0xFF00
#define LOWER 0x00FF


//8MHz max speed since that is clk input to the adc
//MSBfirst
//I think mode0- https://www.tutorialspoint.com/arduino/arduino_serial_peripheral_interface.htm
SPISettings setting1(8000000, MSBFIRST, SPI_MODE0)


//Data arrays//////////////////////////////////////////////////
int sen_array[8][50];
byte packet[17];

//Constants////////////////////////////////////////////////////
const short threshold = 99; //TBD value based on experimentation
const uint8_t identifier = 99; //placeholder
int top = 5;  //point at which we identify "good data"

//Temps or flags//////////////////////////////////////////////////
int i, j, max_index, time_index;
short peak;
bool collision_flag;
byte index = 0;
byte maxIndex, high_sens;
int maxValue, max_num;

void setup() {
  // put your setup code here, to run once:
  time_index = 0
  collision_flag = false;
  SPI.begin();
  SPI.beginTransaction(setting1);
  pinMode(DRDY, INPUT)


}

void loop() {
  // put your main code here, to run repeatedly:

  //When receiving ready signal 
  if(digitalRead(DRDY) == LOW)
  {
    //read all 8 transfers
    for(i = 0; i < 8; i++)
    {
      sen_array[i][time_index] = SPI.transfer16(0);
      //collision detection
      if( (!collision_flag) && (sensors[i] > threshold) )
      {
        collision_flag = true;
      }      
    }
    //Determines which sensor data is valid for finding collision peak
    if(time_index == top){
      high_sens = 0;
      max_num = 0;
      for(i = 1; i < 8; i++){
        for(j = 0; j < 5; j++){
          if(max_num < sen_array[i][j]){
            max_num = sen_array[i][j];
            high_sens = i
          }
        }
      }
    }
    if(collision_flag){
      time_index++;
    }
    if(collision_flag && (sensors[i] <= threshold))
    {

      //Determines which slice of data to transmit
      maxIndex = 0;
      maxValue = sen_array[high_sens][maxIndex];
      for( i = 1; i < 50; i++)
      {
          if(sen_array[i] > maxValue) {
              maxValue = sen_array[i];
              maxIndex = i;
          }
      }
      
      //concatenate the data in one array
      packet[0] = identifier;
      for(i = 1; i < 17; i+=2)
      {
        packet[i] = sen_array[i][max_index] & UPPER
        packet[i+1] = sen_array[i][max_index] & LOWER
      }

      //TODO: Send data

      collision_flag = false;
    }
  }

}




