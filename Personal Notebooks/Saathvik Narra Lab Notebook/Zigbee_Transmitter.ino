#include <SoftwareSerial.h>
SoftwareSerial xbeeSerial(2,3); //RX, TX

//byte len = 0x10;
byte packet[] = {0x7E, 0x00, 0x10, 0x01, 0x01, 0x11, 0x11, 0x00, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64};
byte team_and_player = 0x01;

byte sen1_part_1 = 0x02;
byte sen1_part_2 = 0x03;

byte sen2_part_1 = 0x04;
byte sen2_part_2 = 0x05;

byte sen3_part_1 = 0x06;
byte sen3_part_2 = 0x07;

byte sen4_part_1 = 0x08;
byte sen4_part_2 = 0x09;

byte sen5_part_1 = 0x0A;
byte sen5_part_2 = 0x0B;

byte sen6_part_1 = 0x0C;
byte sen6_part_2 = 0x0D;

byte sen7_part_1 = 0x0E;
byte sen7_part_2 = 0x0F;

byte sen8_part_1 = 0x10;
byte sen8_part_2 = 0x11;

byte checksum = 0x00;

//byte basic_packet[] = {0x7E, 0x00, 0x0C, 0x01, 0x01, 0x11, 0x11, 0x00, team_and_player, sen1_part_1, sen1_part_2, sen2_part_1, sen2_part_2, sen3_part_1, sen3_part_2, checksum};
byte basic_packet[] = {0x7E, 0x00, 0x16, 0x01, 0x01, 0x11, 0x11, 0x00, team_and_player, sen1_part_1, sen1_part_2, sen2_part_1, sen2_part_2, sen3_part_1, sen3_part_2, sen4_part_1, sen4_part_2, sen5_part_1, sen5_part_2, sen6_part_1, sen6_part_2, sen7_part_1, sen7_part_2, sen8_part_1, sen8_part_2, checksum};
//, 0xBF

void setup() {
   Serial.begin(9600);
   xbeeSerial.begin(9600);

   delay(1000);
   Serial.println("Starting up");
//    for(int i=3;i<(sizeof(packet));i++)
//    {
//        Serial.println(packet[i], HEX);
//        checksum += packet[i]; //xbeeFrame is the array where are stored the bytes' frame
//    }
//    
////    checksum &= 0xff; // This one is done implicit through the byte type
//    checksum = 0xff - checksum; // Noted in your example
//    
////    Serial.println(checksum, HEX);
//    packet[sizeof(packet)-1] = checksum;
//    Serial.println(packet[sizeof(packet)-1], HEX);

//Test Inputs///////////////////////////////
  team_and_player = 0x64;

  sen1_part_1 = 0x69;
  sen1_part_2 = 0x73; 

  sen2_part_1 = 0x68;
  sen2_part_2 = 0x65;

  sen3_part_1 = 0x6C;
  sen3_part_2 = 0x6D;

  sen4_part_1 = 0x01;
  sen4_part_2 = 0x02; 

  sen5_part_1 = 0x03;
  sen5_part_2 = 0x04;

  sen6_part_1 = 0x05;
  sen6_part_2 = 0x06;

  sen7_part_1 = 0x07;
  sen7_part_2 = 0x08;

  sen8_part_1 = 0x09;
  sen8_part_2 = 0x0A;
  ///////////////////////////////////////////////////////////////////////////////

  //making the packet///////////////////////////////////////////////////////////
  basic_packet[8] = team_and_player;
  basic_packet[9] = sen1_part_1;
  basic_packet[10] = sen1_part_2;
  basic_packet[11] = sen2_part_1;
  basic_packet[12] = sen2_part_2;
  basic_packet[13] = sen3_part_1;
  basic_packet[14] = sen3_part_2;
  basic_packet[15] = sen4_part_1;
  basic_packet[16] = sen4_part_2;
  basic_packet[17] = sen5_part_1;
  basic_packet[18] = sen5_part_2;
  basic_packet[19] = sen6_part_1;
  basic_packet[20] = sen6_part_2;
  basic_packet[21] = sen7_part_1;
  basic_packet[22] = sen7_part_2;
  basic_packet[23] = sen8_part_1;
  basic_packet[24] = sen8_part_2;
    
    checksum = 0x00;
    for(int i=3;i<(sizeof(basic_packet)-1);i++){
//        Serial.println(basic_packet[i], HEX);
        checksum += basic_packet[i]; 
    }
    checksum = 0xff - checksum;
    
//    Serial.println(checksum, HEX);
//    basic_packet[sizeof(basic_packet)-1] = checksum;
    basic_packet[sizeof(basic_packet)-1] = checksum;
//    Serial.println(basic_packet[sizeof(basic_packet)-1], HEX);
  //////////////////////////////////////////////////////////////////////////////////

}


void loop() {
//  //Test Inputs///////////////////////////////
//  team_and_player = 0x11;
//
//  sen1_part_1 = 0x12;
//  sen1_part_2 = 0x13; 
//
//  sen2_part_1 = 0x14;
//  sen2_part_2 = 0x15;
//
//  sen3_part_1 = 0x16;
//  sen3_part_2 = 0x17;
//  ///////////////////////////////////////////////////////////////////////////////
//
//  //making the packet///////////////////////////////////////////////////////////
//  basic_packet[8] = team_and_player;
//  basic_packet[9] = sen1_part_1;
//  basic_packet[10] = sen1_part_2;
//  basic_packet[11] = sen2_part_1;
//  basic_packet[12] = sen2_part_2;
//  basic_packet[13] = sen3_part_1;
//  basic_packet[14] = sen3_part_2;
//    
//    checksum = 0x00;
//    for(int i=3;i<(sizeof(basic_packet)-1);i++){
//        Serial.println(basic_packet[i], HEX);
//        checksum += basic_packet[i]; 
//    }
//    checksum = 0xff - checksum;
//    
//    Serial.println(checksum, HEX);
////    basic_packet[sizeof(basic_packet)-1] = checksum;
//    basic_packet[sizeof(basic_packet)-1] = checksum;
////    Serial.println(basic_packet[sizeof(basic_packet)-1], HEX);
//  //////////////////////////////////////////////////////////////////////////////////

  //Sending the packet//////////////////////////////////////////////////////////
  
    delay(1000);
    xbeeSerial.write(basic_packet, sizeof(basic_packet));
    delay(5000);

    
//   if(Serial.available() > 0){
//      xbeeSerial.write(basic_packet, sizeof(basic_packet));
//      delay(5000);
//   }

}
