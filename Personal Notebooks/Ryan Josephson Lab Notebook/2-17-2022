Ryan Josephson 2/17/2023

Objectives
1. Clean up the schematic.
2. Get headway on the ATmega in the schematic.
3. Time permitting, add the Zigbee module to the schematic.


Note: 2.2uF caps are required for the SPX1117 capacitors,
and the 4.7uF caps are decoupling? I used same resistor values
as the ones used from IFE.

1.
I moved the decoupling capacitors off the ADC, and into it's own section.
Each capacitor is named as follows:
    C(Chip#)-(Pin#)
    For example, for the ADC, since it is chip 2, the decoupling capacitor
    on pin 5 would be called: C2-5

Looking at page 17 of the datasheet, the data input can be read from
+Vref to -Vref, and outputs a positive or negative number accordingly.
With this, we need a 2.5V regulator added. It will be used to power the
ADC's Vref as well as the FSR voltage divider circuit. Unfortunately,
we can only scale from +Vref to 0V, so we can only get data values from
0000h to 7FFFh, which is 15 bits of accuracy rather than 16 bits.

Also, I just added the resistors for the voltage divider. The datasheet
for the FSRs that we used said the max recommended current to be used
is 2.5mA, so I can calculate the maximum current that will be felt
at the sense points. The maximum current would be when the FSR is acting
as a short, so I can take 0.0025=Vin/Rparallel, where Vin is the input voltage
and Rparallel is the resistor placed in parallel with the FSRs. Assuming
we use 2.5V input voltage to the voltage divider, Rparallel can be 2.5/0.0025
at least, so I'm going to use 2.5/0.00125 to play it safe. This value would be
2000ohms.

As of right now, the only pins of the ADC that I'm unsure of are CLKDIV, SYNC',
and logic 1.

Also, let's ask Ugur if we want separate 2.5V rails.

2.
I'm having some trouble interfacing the ADC and the ATmega, primarily with the
parallel outputs of the ADC. I'm sending an email to Ugur about this.

3.
Unfortunately, time was not permitting. Once I fix the interfacing problem,
I'll have a better understanding of which pins will be connected to the
Zigbee module.
