#include <SPI.h>



//#define DRDY 31 //PC6 = DRDY = pin 31
#define UPPER 0xFF00
#define LOWER 0x00FF


//Control signals
#define DRDY    PC6
#define SS      PB0
#define SCLK    PB1
#define MOSI    PB2
#define SYNCbar PB4

//DOUT pins
#define DOUT8 PD7
#define DOUT7 PD6
#define DOUT6 PD4
#define DOUT5 PD5
#define DOUT4 PD2
#define DOUT3 PD3
#define DOUT2 PD1
#define DOUT1 PD0

/*
FORMAT[2:0] as:
010 -> SPI Discrete mode -> SPI and 8 parallel DOUTs
001 -> SPI TDM, fixed -> SPI and only on DOUT1
*/
#define FORMAT2 PF4
#define FORMAT1 PF5
#define FORMAT0 PF6

//TEST[1:0]: 00 for normal operation, 11 for test mode
#define TEST1 PB5
#define TEST0 PB6
//MODE = 0 for High-speed mode
#define MODE  PE2

//Zigbee connections
//#define RX PF0
#define RX PB2
#define TX PF1

#define CLOCK_PRESCALER_2 (0x1)

#define SHORT_MAX (0x7FFF)

#define ARR_LENGTH 80

//8MHz max speed since that is clk input to the adc
//MSBfirst
//I think mode0- https://www.tutorialspoint.com/arduino/arduino_serial_peripheral_interface.htm
SPISettings setting1(16000000, MSBFIRST, SPI_MODE0);


//Data arrays//////////////////////////////////////////////////
int sen_array[8][ARR_LENGTH];
int arr1[8];
int arr2[8];
int temp_arr[8];
int* cur;
int* prev;
int* temp_ptr;


byte packet[17];

//Constants////////////////////////////////////////////////////
const short threshold = 32200;     //TBD value based on experimentation
const uint8_t identifier = 99;  //placeholder
const int max_jump = 2000;       //temp value
int top = 5;  //point at which we identify "good data"

//Temps or flags//////////////////////////////////////////////////
int i, j, max_index, time_index;
short peak;
bool collision_flag, collision_flag2;
bool flag1 = true;
byte index = 0;
byte min_idx, high_sens, max_diff;
int min_Val, min_num;
bool calib_flag, bad_calib;
int old_state, new_state;
unsigned long read_num, bad_num;
bool big_jump[ARR_LENGTH]; 

int real_data[ARR_LENGTH];
int stored_prev[8];
int stored_cur[8];

void setup() {
  Serial.begin(57600);
   


  cli();                   // Disable interrupts
  CLKPR = _BV(CLKPCE);     //  Enable change. Write the CLKPCE bit to one and all the other to zero. Within 4 clock cycles, set CLKPR again
  CLKPR = CLOCK_PRESCALER_2; // Change clock division. Write the CLKPS0..3 bits while writing the CLKPE bit to zero
  sei(); 
  
  delay(4000);
  Serial.println("Starting Initialization"); 

  // put your setup code here, to run once:
  time_index = 0;
  bad_num =  0;
  read_num = 0;
  collision_flag = false;
  collision_flag2 = false;

  calib_flag = true;
  bad_calib = false;
  SPI.begin();
  SPI.beginTransaction(setting1);
  new_state = LOW;

  //Configure pins to I/O

  //Configure PB I/O
  DDRB  = B01110111;
  PORTB = B00011001;

  DDRC  = B10000000;
  PORTC = B01000000;

  DDRD  = B00000000;
  PORTD = B11111111;

  DDRE  = B00000100;
  PORTE = B00000100;  

  DDRF  = B11110010;
  PORTF = B10000011; //dynamic TDM SPI, Dynamic because easier 000
  
  cur = arr1;
  prev = arr2;
  for(i = 0; i < 8; i++)
  {
    prev[i] = 32300;
    big_jump[i] = false;
  }


  Serial.println("Initialization Complete");  
  delay(500);
  

}

void loop() {
  // put your main code here, to run repeatedly:

  // old_state = new_state;
  // new_state = digitalRead(DRDY);  
  // Serial.print("Old = ");
  // Serial.println(old_state);
  // Serial.print("New = ");
  // Serial.println(new_state);
  // Serial.println("");


  //When receiving negative edge signal 
  if( digitalRead(DRDY) == LOW )
  {
    // flag1 = false;
    // Serial.println("DRDY Signal Received");
    //read all 8 transfers into "current" array
    cur[0] = SPI.transfer16(0xFFFF);
    cur[1] = SPI.transfer16(0xFFFF);
    cur[2] = SPI.transfer16(0xFFFF);
    cur[3] = SPI.transfer16(0xFFFF);
    cur[4] = SPI.transfer16(0xFFFF);
    cur[5] = SPI.transfer16(0xFFFF);
    cur[6] = SPI.transfer16(0xFFFF);
    cur[7] = SPI.transfer16(0xFFFF);

    read_num++;

    //Calibration- wait until all signals read > 32000 to start regular operation
    if(calib_flag)
    {
      Serial.println("Calibrating...");
      Serial.print("[");
      for(i = 0; i < 8; i++)
      {
        Serial.print(cur[i]);
        Serial.print(", ");
      }
      Serial.println("]");
      delay(500);
      for(i = 0; i < 8; i++)
      {
        //check if all are greater than 32300
        if(cur[i] < 32300)
        {
          bad_calib = true;
          break;
        }
      }
      //if they are not, then wait and sample again
      if(bad_calib)
      {
        bad_calib = false;
        return;
      }
      //if they are, then stop initialization and begin regular operation
      else
      {
        temp_ptr = cur;
        cur = prev;  
        prev = temp_ptr;

        Serial.println("Calibration Complete");
        delay(2000);
        calib_flag = false;
      }
    }

    //Serial print the read data
    // Serial.print("Direct Data Read: [");
    // for(i = 0; i < 8; i++)
    // {
    //   //If we have too big of a negative noise, it can cause the jump checker to overflow
    //   //This is a workaround, since a negative number should never be valid data
    //   if(cur[i] < 0)
    //   {
    //     cur[i] = 0;
    //   }
    //   Serial.print(cur[i]);
    //   Serial.print(", ");
    // }
    // Serial.println("]");


    //Regular operation

    //Check for unacceptable jump
    for( i = 0; i < 8; i++)
    {
      // stored_cur[i] = cur[i];
      // stored_prev[i] = prev[i];
      //if the step size is outside an acceptable range, set the previous value as the current value
      if( (cur[i] - prev[i] > max_jump) || (prev[i] - cur[i] > max_jump) )
      {
        // if(collision_flag)
        // { 
        //   if(i == 0)
        //   {
        //     big_jump[time_index] = true;
        //     real_data[time_index] = cur[i];
        //   }
        // }
        // cur[i] = prev[i];
        bad_num++;
        return;
      }
      // else if(collision_flag)
      // {
      //   if(i == 0)
      //   {
      //     big_jump[time_index] = false;
      //     real_data[time_index] = cur[i];
      //   }
      // }
    }
    //counting total number of reads
    

    //Serial print the received and modified data
    // Serial.print("Post-Filtering Data: [");
    // for(i = 0; i < 8; i++)
    // {
    //   Serial.print(cur[i]);
    //   Serial.print(", "); 
    // }
    // Serial.println("]");
    // Serial.print("Total Reads: ");
    // Serial.println(read_num);
    // Serial.print("Total Skips: ");
    // Serial.println(bad_num);
    // Serial.println("");
    // delay(50);

    // Collision starting
    // temporarily cur[0], will need to check all for full functionality
    if( (!collision_flag) && (cur[0] < threshold) )
    {
      collision_flag = true;
    }      
    else if(collision_flag && !collision_flag2 && (cur[0] < threshold))
    {
      collision_flag2 = true;
    }
    else if(collision_flag && !collision_flag2 && (cur[0] >= threshold))
    {
      collision_flag = false;
      time_index = 0;
    }
    //Determines which sensor data is valid for finding collision peak
    // if(time_index == top){
    //   high_sens = 0;
    //   min_num = 32767;
    //   for(i = 1; i < 8; i++){
    //     for(j = 0; j < 5; j++){
    //       if(min_num > sen_array[i][j]){
    //         min_num = sen_array[i][j];
    //         high_sens = i
    //       }
    //     }
    //   }
    // }
    //Collision handling, write into 2D array
    if(collision_flag){
      for(i = 0; i < 8; i++)
      {
      sen_array[i][time_index]=cur[i];
      }
      time_index++;
      // delay(3);
    }

    //Collision ending 
    if((collision_flag2 && (cur[0] >= threshold)) || time_index==ARR_LENGTH)
    {

      //Determines which slice of data to transmit
      max_diff = 0;
      min_idx = 0;
      min_Val = SHORT_MAX;
      Serial.print("[ ");
      for( i = 0; i < ARR_LENGTH; i++)
      {
        if(sen_array[0][i] < 100)
        {
          break;
        }
          if(sen_array[0][i] < min_Val) {
              min_Val = sen_array[0][i];
              min_idx = i;
          }
          Serial.print(sen_array[0][i]);
          Serial.print(", ");
      }
      Serial.println(" ]");

      Serial.print("[ ");
      for( i = 0; i < ARR_LENGTH; i++)
      {
        if(sen_array[0][i] < 100)
        {
          break;
        }
          Serial.print(big_jump[i]);
          Serial.print(", ");
      }

      Serial.println(" ]");

      Serial.print("[ ");
      for( i = 0; i < ARR_LENGTH; i++)
      {
        if(sen_array[0][i] < 100)
        {
          break;
        }
          Serial.print(real_data[i]);
          Serial.print(", ");
      }

      Serial.println(" ]");
      Serial.print("min_Val: ");
      Serial.println(min_Val);
      // Serial.print("max_diff: ");
      // Serial.println(max_diff);
      
      delay(9999999);
      // //concatenate the data in one array
      // packet[0] = identifier;
      // for(i = 1; i < 17; i+=2)
      // {
        // packet[i] = (sen_array[i][min_idx] & UPPER) >> 8;
        // packet[i+1] = sen_array[i][min_idx] & LOWER;
      // }

      // //TODO: Send data

      collision_flag = false;
      
    }//Close Collision End


    //set the 'cur' array as the 'prev' array for the next iteration
    //this code will probably need to be moved when we modify functionality
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    temp_ptr = cur;
    cur = prev;  
    prev = temp_ptr;
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    // delay(250);
  } //Close DRDY
  
} //Close loop




// if(cur[i] < 0)
//       {
//         Serial.println("");
//         Serial.print("Stored Cur-");
//         Serial.println(stored_cur[i]);
//         Serial.print("Stored Prev-");
//         Serial.println(stored_prev[i]);
//         Serial.print("Cur - Prev = ");
//         Serial.println(stored_cur[i] - stored_prev[i]);
//         Serial.print("Prev - Cur = ");
//         Serial.println(stored_prev[i] - stored_cur[i]);

//         Serial.println(big_jump[i]);
//         for(i = 0; i < 8; i++)
//         {
//           Serial.print(bad_num[i]);
//           Serial.print(", ");
//         }
//         Serial.println(read_num);
//         delay(99999);
//       }

       // if(i > 0)
        // {
        //   if(sen_array[0][i] - sen_array[0][i-1] > max_diff)
        //   {
        //     max_diff = sen_array[0][i] - sen_array[0][i-1];
        //   }
        //   else if(sen_array[0][i-1] - sen_array[0][i] > max_diff)
        //   {
        //     max_diff = sen_array[0][i-1] - sen_array[0][i];
        //   }
        // }

